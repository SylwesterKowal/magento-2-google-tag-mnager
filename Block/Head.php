<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoogleTagManager\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

class Head extends Template
{

    /**
     * Head constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue('kowal_google_tag_manager/main/enable', ScopeInterface::SCOPE_STORE);
    }

    public function gtmId()
    {
        return $this->scopeConfig->getValue('kowal_google_tag_manager/main/gtmid', ScopeInterface::SCOPE_STORE);
    }
}

