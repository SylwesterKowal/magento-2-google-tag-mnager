# Mage2 Module Kowal GoogleTagManager

    ``kowal/module-googletagmanager``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Dodanie GTM do strony

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_GoogleTagManager`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-googletagmanager`
 - enable the module by running `php bin/magento module:enable Kowal_GoogleTagManager`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (kowal_google_tag_manager/main/enable)

 - head (kowal_google_tag_manager/content/head)

 - body (kowal_google_tag_manager/content/body)


## Specifications

 - Block
	- Head > head.phtml

 - Block
	- Body > body.phtml


## Attributes



